#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QString>
#include <QDateTime>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QByteArray>

//!
//! \brief Клиент.
//! Создает подключение к серверу, принимает сообщения и отправляет свои.
//!
class Client : public QObject
{
    Q_OBJECT

public:
    explicit Client(QObject *parent = 0);

    //!
    //! \brief Информация о подключении.
    //! \return Статус подключения.
    //!
    bool isConnected();

signals:
    //!
    //! \brief Сигнал - получено сообщение.
    //! \param date Дата сообщения (UTC).
    //! \param name Имя клиента.
    //! \param message Текст сообщения.
    //!
    void messageReceived(const QDateTime &date, const QString &name, const QString &message);

    //!
    //! \brief Сигнал - клиент подключился к серверу.
    //!
    void connected();

    //!
    //! \brief Сигнал - клиент отключился от сервера.
    //!
    void disconnected();

public slots:
    //!
    //! \brief Подключиться к серверу.
    //! \param hostName Сетевой адрес.
    //! \param port Порт.
    //!
    void connectToHost(const QString &hostName, quint16 port);

    //!
    //! \brief Отключиться от сервера.
    //!
    void disconnectFromHost();

    //!
    //! \brief Послать имя клиента.
    //! \param name Имя клиента.
    //!
    void sendName(const QString &name);

    //!
    //! \brief Послать сообщение.
    //! \param message Текст сообщения.
    //!
    void sendMessage(const QString &message);

private:
    QTcpSocket *socket;
    QByteArray buf;

private slots:
    // Сокет изменил состояние
    void onStateChanged(QAbstractSocket::SocketState socketState);

    // Данные готовы к получению
    void onReadyRead();

    // Передача данных окончена
    void onBytesWritten(qint64 bytes);

    // Сокет получил сигнал закрытия подключения
    void onAboutToClose();
};

#endif // CLIENT_H
