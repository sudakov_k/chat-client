#-------------------------------------------------
#
# Project created by QtCreator 2016-09-25T14:46:32
#
#-------------------------------------------------

QT       += core gui network
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = chat-client
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    protocol.cpp \
    client.cpp

HEADERS  += mainwindow.h \
    protocol.h \
    client.h

FORMS    += mainwindow.ui
