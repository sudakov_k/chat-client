#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <QtGlobal>
#include <QString>
#include <QByteArray>

// TODO Предусмотреть прием склееных посылок.

//!
//! \brief Протокол передачи сообщений.
//!
class Protocol
{
public:
    //!
    //! \brief Тип ошибки при декодировании посылки.
    //!
    enum class Error {
        Ok = 0,                             //!< Нет ошибки.
        Truncated,                          //!< Попытка декодировать часть посылки.
        Fail,                               //!< Неправильная посылка.
    };

    //!
    //! \brief Тип посылки сервера.
    //!
    enum class ServerCommand : quint16 {
        Message = 1,                        //!< Сообщение.
    };

    //!
    //! \brief Тип посылки клиента.
    //!
    enum class ClientCommand : quint16 {
        Name = 1,                           //!< Имя клиента.
        Message = 2,                        //!< Сообщение.
    };

    //!
    //! \brief Декодирует сообщение сервера.
    //! \param data Входные данные.
    //! \param name Имя клиента, который послал сообщение.
    //! \param message Текст сообщения.
    //! \param size Размер успешно декодированных данных.
    //! \return Ошибка декодирования.
    //!
    static Error decodeMessage(const QByteArray &data, QString &name, QString &message, int &size);

    static QByteArray encodeName(const QString &name);
    static QByteArray encodeMessage(const QString &message);

private:
    // Преобразование "сырых" данных в тип uint16_t
    static quint16 toU16(char high, char low);

    // Преобразование типа uint16_t в массив данных
    static QByteArray fromU16(quint16 value);

    // Функция используется другими функциями для кодирования сообщений
    static QByteArray encodeData(ClientCommand cmd, const QByteArray &data);
};

#endif // PROTOCOL_H
