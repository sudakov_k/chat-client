#include <cstdint>
#include "protocol.h"

Protocol::Error Protocol::decodeMessage(const QByteArray &data, QString &name, QString &message, int &size)
{
    Error err = Error::Truncated;   // До завершения декодирования считается, что посылка пришла не полностью
    int minSize = 2;                // Минимальный размер посылки

    // Обработка секции "cmd"
    if (data.size() >= minSize) {
        ServerCommand cmd = static_cast<ServerCommand>(toU16(data[0], data[1]));

        switch (cmd) {
        case ServerCommand::Message:
            // Тип посылки - сообщение, продолжение обработки посылки
            // Добавлен размер секций "name size" и "message size"
            minSize += 4;
            if (data.size() >= minSize) {
                quint16 nameSize = toU16(data[2], data[3]);
                minSize += nameSize;

                if (data.size() >= minSize) {
                    // Получение имени клиента
                    // Для увеличения производительности мождно ипользовать функцию
                    // QString::fromUtf8(const char *str, int size = -1),
                    // но из документации данной функции:
                    // "Returns a QString initialized with the first size bytes of the UTF-8 string str",
                    // кажется, что size относится к инициализации строки, а не является размером
                    // входных данных
                    name = QString::fromUtf8(data.mid(4, nameSize));

                    quint16 messageSize = toU16(data[4 + nameSize], data[5 + nameSize]);
                    minSize += messageSize;

                    if (data.size() >= minSize) {
                        // Получение текста сообщения
                        // Для увеличения производительности мождно ипользовать функцию
                        // QString::fromUtf8(const char *str, int size = -1),
                        // но из документации данной функции:
                        // "Returns a QString initialized with the first size bytes of the UTF-8 string str",
                        // кажется, что size относится к инициализации строки, а не является размером
                        // входных данных
                        message = QString::fromUtf8(data.mid(6 + nameSize, messageSize));

                        // Декодирование закончено
                        err = Error::Ok;
                    }
                }
            }

            break;

        default:
            // Посылка имеет некорректный тип
            err = Error::Fail;
            break;
        }
    }

    if (err == Error::Ok) {
        size = minSize;
    } else {
        size = 0;
    }

    return err;
}

QByteArray Protocol::encodeName(const QString &name)
{
    return encodeData(ClientCommand::Name, name.toUtf8());
}

QByteArray Protocol::encodeMessage(const QString &message)
{
    return encodeData(ClientCommand::Message, message.toUtf8());
}

quint16 Protocol::toU16(char high, char low)
{
    // Преобразование char -> unsigned char необходимо
    // для избежания ошибок преобразования отрицательных чисел
    quint16 value = static_cast<unsigned char>(low);
    value |= (static_cast<quint16>(static_cast<unsigned char>(high)) << 8) & 0xFF00;

    return value;
}

QByteArray Protocol::fromU16(quint16 value)
{
    QByteArray raw;
    raw.reserve(2);

    raw.append(static_cast<char>(static_cast<unsigned char>((value >> 8) & 0xFF)));
    raw.append(static_cast<char>(static_cast<unsigned char>(value & 0xFF)));

    return raw;
}

QByteArray Protocol::encodeData(ClientCommand cmd, const QByteArray &data)
{
    QByteArray outData;
    outData.reserve(4 + data.size());

    switch (cmd) {
    case ClientCommand::Name:
    case ClientCommand::Message:
        if (data.size() <= UINT16_MAX) {
            outData.append(fromU16(static_cast<quint16>(cmd))); // cmd
            outData.append(fromU16(data.size()));               // data size
            outData.append(data);                               // data
        } else {
            outData.squeeze();
        }

        break;
    default:
        outData.squeeze();
        break;
    }

    return outData;
}
