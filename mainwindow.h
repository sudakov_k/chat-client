#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "client.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Client *client;

private slots:
    void on_connectButton_clicked();
    void on_sendButton_clicked();

    // Получение сообщения
    void onMessageRecieved(const QDateTime &date, const QString &name, const QString &message);

    // Обработка подключения к серверу
    void onConnected();

    // Обработка отключения от сервера
    void onDisconnected();
};

#endif // MAINWINDOW_H
