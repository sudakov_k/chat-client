#include <QColor>
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    client(new Client(this))
{
    ui->setupUi(this);

    connect(client, &Client::messageReceived, this, &MainWindow::onMessageRecieved);
    connect(client, &Client::connected, this, &MainWindow::onConnected);
    connect(client, &Client::disconnected, this, &MainWindow::onDisconnected);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_connectButton_clicked()
{
    // Блокировка кнопки, разблокируется по завершении операции
    ui->connectButton->setEnabled(false);

    QColor oldColor = ui->textEdit->textColor();

    if (!client->isConnected()) {
        ui->textEdit->setTextColor(Qt::darkBlue);
        ui->textEdit->append("подключение");
        ui->textEdit->setTextColor(oldColor);

        client->connectToHost(ui->serverEdit->text(), ui->portEdit->text().toUInt());
    } else {
        ui->textEdit->setTextColor(Qt::darkRed);
        ui->textEdit->append("отключение");
        ui->textEdit->setTextColor(oldColor);

        client->disconnectFromHost();
    }
}

void MainWindow::on_sendButton_clicked()
{
    if (client->isConnected()) {
        client->sendMessage(ui->sendEdit->toPlainText());
        ui->sendEdit->clear();
    }
}

void MainWindow::onMessageRecieved(const QDateTime &date, const QString &name, const QString &message)
{
    QString text = QString("%1 [%2] %3").arg(
                date.toLocalTime().toString("yyyy-MM-dd HH:mm:ss"),
                name,
                message);

    ui->textEdit->append(text);
}

void MainWindow::onConnected()
{
    QColor oldColor = ui->textEdit->textColor();
    ui->textEdit->setTextColor(Qt::darkBlue);
    ui->textEdit->append("клиент подключен");
    ui->textEdit->setTextColor(oldColor);

    ui->connectButton->setText(tr("Отключить"));
    ui->connectButton->setEnabled(true);

    ui->serverEdit->setEnabled(false);
    ui->portEdit->setEnabled(false);
    ui->nameEdit->setEnabled(false);
    ui->sendButton->setEnabled(true);

    client->sendName(ui->nameEdit->text());
}

void MainWindow::onDisconnected()
{
    QColor oldColor = ui->textEdit->textColor();
    ui->textEdit->setTextColor(Qt::darkRed);
    ui->textEdit->append("клиент отключен");
    ui->textEdit->setTextColor(oldColor);

    ui->connectButton->setText(tr("Подключить"));
    ui->connectButton->setEnabled(true);

    ui->serverEdit->setEnabled(true);
    ui->portEdit->setEnabled(true);
    ui->nameEdit->setEnabled(true);
    ui->sendButton->setEnabled(false);
}
