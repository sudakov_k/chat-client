#include "client.h"
#include "protocol.h"

#include <QDebug>

Client::Client(QObject *parent) :
    QObject(parent),
    socket(new QTcpSocket(this))
{
    connect(socket, &QTcpSocket::stateChanged, this, onStateChanged);
    connect(socket, &QTcpSocket::readyRead, this, onReadyRead);
    connect(socket, &QTcpSocket::bytesWritten, this, onBytesWritten);
    connect(socket, &QTcpSocket::aboutToClose, this, onAboutToClose);
}

bool Client::isConnected()
{
    return socket->state() == QAbstractSocket::ConnectedState;
}

void Client::connectToHost(const QString &hostName, quint16 port)
{
    // TODO Узнать о необходимости преобразования национальных доменов.
    socket->connectToHost(hostName, port);
}

void Client::disconnectFromHost()
{
    socket->disconnectFromHost();
}

void Client::sendName(const QString &name)
{
    if (isConnected()) {
        socket->write(Protocol::encodeName(name));
    }
}

void Client::sendMessage(const QString &message)
{
    if (isConnected()) {
        socket->write(Protocol::encodeMessage(message));
    }
}

void Client::onStateChanged(QAbstractSocket::SocketState socketState)
{
    qDebug() << Q_FUNC_INFO << socketState;

    switch (socketState) {
    case QAbstractSocket::ConnectedState:
        buf.clear();
        emit connected();
        break;

    case QAbstractSocket::UnconnectedState:
        buf.clear();
        emit disconnected();
        break;

    default:
        break;
    }
}

void Client::onReadyRead()
{
    qDebug() << Q_FUNC_INFO;

    buf.append(socket->readAll());

    int recvSize = 0;
    QString name;
    QString message;

    do {
        Protocol::Error err = Protocol::decodeMessage(buf, name, message, recvSize);

        switch (err) {
        case Protocol::Error::Ok:
            emit messageReceived(QDateTime::currentDateTimeUtc(), name, message);
            buf.remove(0, recvSize);
            break;

        case Protocol::Error::Truncated:
            break;

        case Protocol::Error::Fail:
        default:
            disconnectFromHost();
            break;
        }
    } while (recvSize != 0);
}

void Client::onBytesWritten(qint64 bytes)
{
    qDebug() << Q_FUNC_INFO << bytes;
}

void Client::onAboutToClose()
{
    qDebug() << Q_FUNC_INFO;
}
